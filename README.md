# IIS图片水印

#### 项目介绍
通过IIS Handler的方式给图片添加水印

#### 软件架构
C#


#### 安装教程

1. 将编译后的DLL放在bin目录下

2. 修改web.config

   ```xml
   <system.webServer>
           <handlers>
               <add name="watermark" verb="GET" path="*.jpg" type="Watermark.WaterMarkHandler, Watermark" />
           </handlers>
   </system.webServer>
   ```

   ​

3. config配置

   ```xml
   <?xml version="1.0" encoding="utf-8" ?>
   <watermark isOpen="1" filePath="~/Images/watermark.png" padding="100" pos="6" quality="90" transparency="90">
     <rule urlfrom="/photo/e0" exclude="1"></rule>
     <rule urlfrom="/photo/f0"></rule>
     <rule urlfrom="/photo/a0"></rule>
   </watermark>
   ```

   rule表示符合路径的都添加水印

   exclude=1表示符合路径的会被排除

   padding 暂时无效

   pos 表示九宫格的位置

   quality 图片压缩的质量

   transparency 水印透明度
