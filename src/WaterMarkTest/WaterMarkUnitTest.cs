﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using Watermark;

namespace WaterMarkTest
{
    [TestClass]
    public class WaterMarkUnitTest
    {
        [TestMethod]
        public void ConfigTest()
        {
            var config = WaterMarkConfig.GetInstance();
            Assert.AreEqual(config.quality, 90);
            Assert.AreEqual(config.quality, 90);
            Assert.AreEqual(config.contain.Count, 2);
            Assert.AreEqual(config.exclude.Count, 1);
        }
    }
}
